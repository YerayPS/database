from sqlalchemy import Column, String
from sqlalchemy.dialects.mysql import INTEGER

from connection import db


class User(db.Model):
    __table_args__ = {"schema": "EMBOU_DATA"}
    __tablename__ = 'EMBOUFD_CONTRACT_DOCS_TYPE'
    id = Column('id', INTEGER, primary_key=True, autoincrement=True)
    code = Column('code', String(40), nullable=True)
    description = Column('description', String(400), nullable=True)
    acronym = Column('acronym', String(45), nullable=True)

    @staticmethod
    def selectAllPaginated(page):
        paginateDataList = []
        paginator = User.query.paginate(page=page, per_page=5)

        for row in paginator.items:
            print(f'{row.id} {row.description}')
            paginateDataList.append(dict(id=row.id, code=row.code, description=row.description, acronym=row.acronym))

        data = dict(total=paginator.total,
                    current_page=paginator.page,
                    per_page=paginator.per_page,
                    pages=paginator.pages)
        print(data)
        return paginateDataList